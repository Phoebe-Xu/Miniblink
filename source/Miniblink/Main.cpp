#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <Commctrl.h>
#include <Shlwapi.h>
#include <strsafe.h>
#include <iostream>
#include <string>
#include <cstdio>
#include "wke.h"
#include "MiniblinkWnd.h"
#include "CallCenter.h"
#include "EventHandle.h"
#include "JsCpp.h"

#pragma comment(lib, "Shlwapi.lib")

#define TEXT_NODE_FILE_NAME		L"node.dll"
#define TEXT_EXTENSION_HTML		L".html"
#define TEXT_HTTP				L"http"
#define TEXT_DEFAULT_URL		L"app:///index.html"
#define TEXT_DEFAULT_CAPTION	L"Miniblink"
#define TEXT_READ_ME_FILE		L"readme.txt"
#define WND_DEFAULT_RECT		{ 400,300,1200,900 }
bool InitMiniblinkLibrary(LPCWSTR lpszDllPath, wkeSettings* p)
{
	bool bSuccess = false;
	static HMODULE hMod = NULL;// hMod 要跟宏 WKE_GET_PTR_ITERATOR 里面的变量命名保持一致
	if (NULL == hMod)
	{
		// 当要加载的DLL路径不在当前目录下时，直接是加载不上的。
		// 要先将要加载的DLL所在目录，修改为当前目录，加载完毕后再还原当前目录。
		WCHAR szCurrentDir[MAX_PATH] = { 0 };
		GetCurrentDirectoryW(__crt_countof(szCurrentDir), szCurrentDir);
		WCHAR szDllDir[MAX_PATH] = { 0 };
		StringCchCopyW(szDllDir, __crt_countof(szDllDir), lpszDllPath);
		PathRemoveFileSpecW(szDllDir);
		SetCurrentDirectoryW(szDllDir);

		// NULL 和 nullptr 混用，是为了遵循各自 C++ 代码规范。
		// MSDN 文档使用 NULL，但 NULL 并不总是表示指针。
		// modern C++ 使用 nullptr 表示指针。
		FN_wkeInitializeEx wkeInitializeExFunc = nullptr;
		if (NULL != (hMod = LoadLibraryW(lpszDllPath)) && nullptr != (wkeInitializeExFunc = (FN_wkeInitializeEx)GetProcAddress(hMod, "wkeInitializeEx")))
		{
			bSuccess = true;
			wkeInitializeExFunc(p);

			WKE_FOR_EACH_DEFINE_FUNCTION(WKE_GET_PTR_ITERATOR0, WKE_GET_PTR_ITERATOR1, WKE_GET_PTR_ITERATOR2, WKE_GET_PTR_ITERATOR3, \
				WKE_GET_PTR_ITERATOR4, WKE_GET_PTR_ITERATOR5, WKE_GET_PTR_ITERATOR6, WKE_GET_PTR_ITERATOR9, WKE_GET_PTR_ITERATOR10, WKE_GET_PTR_ITERATOR11);
		}

		SetCurrentDirectoryW(szCurrentDir);
	}
	return bSuccess;
}

LPCWSTR Url(HINSTANCE hInstance)
{
	static WCHAR szPath[MAX_PATH] = { 0 };
	auto funcLocalHtml = [&]()->bool
	{
		bool bSuccess = true;
		GetModuleFileNameW(hInstance, szPath, __crt_countof(szPath));
		PathRemoveExtensionW(szPath);
		PathAddExtensionW(szPath, TEXT_EXTENSION_HTML);
		if (FALSE == PathFileExistsW(szPath))
		{
			bSuccess = false;
		}
		return bSuccess;
	};
	auto funcConfigUrl = [&]()->bool
	{
		bool bSuccess = false;
		GetModuleFileNameW(hInstance, szPath, __crt_countof(szPath));
		PathRemoveFileSpecW(szPath);
		PathAppendW(szPath, TEXT_READ_ME_FILE);
		if (TRUE == PathFileExistsW(szPath))
		{
			FILE* file = NULL;
			_wfopen_s(&file, szPath, L"r");
			if (NULL != fgetws(szPath, __crt_countof(szPath), file) && 0 == _wcsnicmp(szPath, TEXT_HTTP, _countof(TEXT_HTTP) - 1))
			{
				bSuccess = true;
				size_t nLen = wcslen(szPath);
				while (0 < nLen && (L' ' == szPath[nLen - 1] || L'\r' == szPath[nLen - 1] || L'\n' == szPath[nLen - 1]))
				{
					--nLen;
					szPath[nLen] = L'\0';
				}
			}
			fclose(file);
			file = NULL;
		}
		return bSuccess;
	};
	if (false == funcConfigUrl() && false == funcLocalHtml())
	{
		StringCchCopyW(szPath, __crt_countof(szPath), TEXT_DEFAULT_URL);
	}
	return szPath;
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
#if 0
	WCHAR szPath[] = L"Z:\\E\\Users\\Phoebe\\Downloads\\miniblink-20220405\\node.dll";
#else
	WCHAR szPath[MAX_PATH] = { 0 };
	GetModuleFileNameW(hInstance, szPath, __crt_countof(szPath));
	PathRemoveFileSpecW(szPath);
	PathAppendW(szPath, TEXT_NODE_FILE_NAME);
#endif

	if (true == InitMiniblinkLibrary(szPath, nullptr))
	{
		CMiniblinkWnd wndMain;
		if (true == wndMain.CreateWebView(WKE_WINDOW_TYPE_POPUP/*WKE_WINDOW_TYPE_TRANSPARENT*/, NULL, WND_DEFAULT_RECT))
		{
			wndMain.SetCaption(TEXT_DEFAULT_CAPTION);
			wndMain.SetResourceProvider(nullptr);
			wndMain.SetQuitAppOnClose(true);
			wndMain.SetVisible(true);

			CEventHandle eventHandle([&wndMain]()->wkeWebView {return wndMain.WebView(); });
			CCallCenter callCenter([&wndMain]()->wkeWebView {return wndMain.WebView(); });

			callCenter.ProvideJsFunc(JSCALLCPP_GET_TEXT, [&callCenter, &eventHandle]()
			{
				std::wstring strTEXT = eventHandle.HandleGetText();
				callCenter.CallJs(CPPCALLJS_SET_TEXT, strTEXT);
			});
#ifdef EXPAND_PARAMETER_LIST
			callCenter.ProvideJsFunc(JSCALLCPP_SET_TITLE, [&eventHandle](std::wstring strTitle) { eventHandle.HandleSetTitle(strTitle); });
			callCenter.ProvideJsFunc(JSCALLCPP_LEFT_BUTTON_DOWN, [&eventHandle](int x, int y) { eventHandle.HandleLButtonDown(x, y); });
			callCenter.ProvideJsFunc(JSCALLCPP_MOUSE_MOVE, [&eventHandle](int x, int y) { eventHandle.HandleMouseMove(x, y); });
			callCenter.ProvideJsFunc(JSCALLCPP_LEFT_BUTTON_UP, [&eventHandle](int x, int y) { eventHandle.HandleLButtonUp(x, y); });
			callCenter.ProvideJsFunc(JSCALLCPP_RIGHT_BUTTON_UP, [&eventHandle](int x, int y) {eventHandle.HandleRButtonUp(x, y); });
#else
			callCenter.ProvideJsFunc(JSCALLCPP_SET_TITLE, 1, [&eventHandle](std::vector<JsValueExtractor>& vecValue) { eventHandle.HandleSetTitle(vecValue[0]); });
			callCenter.ProvideJsFunc(JSCALLCPP_LEFT_BUTTON_DOWN, 2, [&eventHandle](std::vector<JsValueExtractor>& vecValue) { eventHandle.HandleLButtonDown(vecValue[0], vecValue[1]); });
			callCenter.ProvideJsFunc(JSCALLCPP_MOUSE_MOVE, 2, [&eventHandle](std::vector<JsValueExtractor>& vecValue) { eventHandle.HandleMouseMove(vecValue[0], vecValue[1]); });
			callCenter.ProvideJsFunc(JSCALLCPP_LEFT_BUTTON_UP, 2, [&eventHandle](std::vector<JsValueExtractor>& vecValue) { eventHandle.HandleLButtonUp(vecValue[0], vecValue[1]); });
			callCenter.ProvideJsFunc(JSCALLCPP_RIGHT_BUTTON_UP, 2, [&eventHandle](std::vector<JsValueExtractor>& vecValue) {eventHandle.HandleRButtonUp(vecValue[0], vecValue[1]); });
#endif
			wndMain.SetJsCppReady([&callCenter]()
			{
				// callCenter.CallJs("alert");
				// callCenter.CallJs("alert", 100);
				// callCenter.CallJs("alert", "The call between JavaScript and C + + has been successfully established.");
				// callCenter.CallJs("alert", L"JavaScript和C++之间的调用已成功建立。");
				callCenter.CallJs(CPPCALLJS_GET_TITLE);
			});

			LPCWSTR lpszUrl = Url(hInstance);
			wndMain.LoadUrl(lpszUrl);

			MSG msg;
			while (TRUE == GetMessageW(&msg, NULL, 0, 0))
			{
				TranslateMessage(&msg);
				DispatchMessageW(&msg);
			}
		}
	}
	return 0;
}