#pragma once
#include "CallCenter.h"
class CEventHandle
{
public:
	CEventHandle(WebViewProvider func);
	virtual ~CEventHandle();

public:
	std::wstring	HandleGetText();
	void			HandleSetTitle(std::wstring strTitle);
	void			HandleLButtonDown(int x, int y);
	void			HandleMouseMove(int x, int y);
	void			HandleLButtonUp(int x, int y);
	void			HandleRButtonUp(int x, int y);

protected:
	HWND			Hwnd();

protected:
	bool	m_bLButtonDown;
	POINT	m_ptLeftBtnDown;
	WebViewProvider	m_funcWebViewProvider;
};

