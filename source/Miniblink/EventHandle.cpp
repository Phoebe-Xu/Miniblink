#include "EventHandle.h"
#include <Shlwapi.h>
#pragma comment(lib, "Shlwapi.lib")

#define DEV_TOOLS_WIDTH				900
#define DEV_TOOLS_HEIGHT			600
#define DEV_TOOLS_CAPTION			u8"DevTools"
#define DEV_TOOLS_PATH				L"inspector\\inspector.html"

#define TEXT_HELLO					L"Hello everybody !"
#define MSG_CAPTION					L"DevTools"
#define MSG_NO_INSPECTOR_FOUND		L"想打开Web调试工具 ?\n请将包含 inspector.html 及其依赖文件的 inspector文件夹 放在当前程序执行目录。"
CEventHandle::CEventHandle(WebViewProvider func)
	: m_bLButtonDown(false)
	, m_ptLeftBtnDown{ 0,0 }
{
	m_funcWebViewProvider = std::move(func);
};

CEventHandle::~CEventHandle()
{
}

std::wstring CEventHandle::HandleGetText()
{
	return TEXT_HELLO;
}

void CEventHandle::HandleSetTitle(std::wstring strTitle)
{
	wkeWebView pWebView = nullptr;
	if (nullptr != m_funcWebViewProvider && nullptr != (pWebView = m_funcWebViewProvider()))
	{
		wkeSetWindowTitleW(pWebView, strTitle.c_str());
	}
}

void CEventHandle::HandleLButtonDown(int x, int y)
{
	m_bLButtonDown = true;
	m_ptLeftBtnDown = { x,y };
	ClientToScreen(Hwnd(), &m_ptLeftBtnDown);
}

void CEventHandle::HandleMouseMove(int x, int y)
{
	if (true == m_bLButtonDown)
	{
		HWND hWnd = Hwnd();
		POINT ptMove = { x,y };
		ClientToScreen(hWnd, &ptMove);
		RECT rcWnd;
		GetWindowRect(hWnd, &rcWnd);
		OffsetRect(&rcWnd, ptMove.x - m_ptLeftBtnDown.x, ptMove.y - m_ptLeftBtnDown.y);
		m_ptLeftBtnDown = ptMove;
		SetWindowPos(hWnd, NULL, rcWnd.left, rcWnd.top, rcWnd.right - rcWnd.left, rcWnd.bottom - rcWnd.top, SWP_NOSIZE);
	}
}

void CEventHandle::HandleLButtonUp(int x, int y)
{
	m_bLButtonDown = false;
}

void WKE_CALL_TYPE onShowDevtoolsCallback(wkeWebView window, void *param)
{
	WCHAR szDir[MAX_PATH] = { 0 };
	GetTempPathW(__crt_countof(szDir), szDir);
	wkeSetLocalStorageFullPath(window, szDir);
	wkeSetCookieJarFullPath(window, szDir);
	// initNetFS(window);
	wkeSetWindowTitle(window, DEV_TOOLS_CAPTION);
	wkeResizeWindow(window, DEV_TOOLS_WIDTH, DEV_TOOLS_HEIGHT);
	wkeMoveToCenter(window);
	const utf8* pszUrl = wkeGetURL(window);
	wkeLoadURL(window, pszUrl);
}

void CEventHandle::HandleRButtonUp(int x, int y)
{
	HWND hWnd = Hwnd();
	wkeWebView pWebView = nullptr;
	if (nullptr != m_funcWebViewProvider && nullptr != (pWebView = m_funcWebViewProvider()))
	{
		WCHAR szPath[MAX_PATH] = { 0 };
		GetModuleFileNameW(NULL, szPath, __crt_countof(szPath));
		PathRemoveFileSpecW(szPath);
		PathAppendW(szPath, DEV_TOOLS_PATH);
		if (TRUE == PathFileExistsW(szPath))
		{
			wkeShowDevtools(pWebView, szPath, onShowDevtoolsCallback, nullptr);
		}
		else
		{
			MessageBoxW(NULL, MSG_NO_INSPECTOR_FOUND, MSG_CAPTION, MB_ICONSTOP | MB_OK);
		}
	}
}

HWND CEventHandle::Hwnd()
{
	HWND hWnd = NULL;
	wkeWebView pWebView = nullptr;
	if (nullptr != m_funcWebViewProvider && nullptr != (pWebView = m_funcWebViewProvider()))
	{
		hWnd = wkeGetWindowHandle(pWebView);
	}
	return hWnd;
}