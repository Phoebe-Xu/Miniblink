﻿#pragma once
#include <iostream>
#include <functional>
#include "wke.h"

using ResourceProvider = std::function<bool(wkeWebView pWebView, const char* pszUrl, wkeNetJob pJob)>;
using CloseConfirmation = std::function<bool()>;
using JsCppReady = std::function<void()>;
class CMiniblinkWnd
{
public:
	CMiniblinkWnd();
	virtual ~CMiniblinkWnd();

	bool			CreateWebView(wkeWindowType emType, HWND hParent, const RECT& rcWnd);
	void			SetCaption(const std::wstring& strCaption);
	void			SetResourceProvider(ResourceProvider funcProvider = nullptr);
	void			SetJsCppReady(JsCppReady = nullptr);
	void			SetCloseConfirmation(CloseConfirmation funcConfirmation = nullptr);
	void			LoadUrl(const std::wstring& strUrl);
	void			SetQuitAppOnClose(bool bQuitApp);
	void			SetVisible(bool bVisible);

	wkeWebView		WebView();

protected:
	static bool WKE_CALL_TYPE HandleLoadUrlBegin(wkeWebView pWebView, void* param, const char* pszUrl, wkeNetJob pJob);
	static void WKE_CALL_TYPE HandleJsCppReady(wkeWebView webView, void* param, const wkeString url, wkeLoadingResult result, const wkeString failedReason);
	static bool	WKE_CALL_TYPE HandleWindowClosing(wkeWebView pWebView, void* param);
	static void	WKE_CALL_TYPE HandleWindowDestroy(wkeWebView pWebView, void* param);

private:
	wkeWebView			m_pWebView;
	bool				m_bQuitOnClose;
	ResourceProvider	m_funcResourceProvider;
	JsCppReady			m_funcJsCppReady;
	CloseConfirmation	m_funcConfirmation;
	std::wstring		m_strUrl;
};

