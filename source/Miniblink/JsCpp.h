#pragma once
#define		JSCALLCPP_GET_TEXT				"getText"
#define		JSCALLCPP_SET_TITLE				"setTitle"
#define		JSCALLCPP_LEFT_BUTTON_DOWN		"leftButtonDown"
#define		JSCALLCPP_MOUSE_MOVE			"mouseMove"
#define		JSCALLCPP_LEFT_BUTTON_UP		"leftButtonUp"
#define		JSCALLCPP_RIGHT_BUTTON_UP		"rightButtonUp"

#define		CPPCALLJS_SET_TEXT				"setText"
#define		CPPCALLJS_GET_TITLE				"getTitle"
