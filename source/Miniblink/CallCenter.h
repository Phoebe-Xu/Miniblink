#pragma once
#ifndef CALL_CENTER_CPP
#define CALL_CENTER_CPP
#include <vector>
#include <functional>
#include <unordered_map>
#include "wke.h"

//#define EXPAND_PARAMETER_LIST
class JsValueExtractor {
public:
	JsValueExtractor(jsExecState pExecState, int nIndex)
		: m_pExecState(pExecState)
		, m_nIndex(jsArg(pExecState, nIndex))
	{
	}
	operator bool() const { return jsToBoolean(m_pExecState, m_nIndex); }
	operator int() const { return jsToInt(m_pExecState, m_nIndex); }
	operator double() const { return jsToDouble(m_pExecState, m_nIndex); }
	operator std::string() const { return jsToTempString(m_pExecState, m_nIndex); }
	operator std::wstring() const { return jsToTempStringW(m_pExecState, m_nIndex); }

private:
	jsExecState m_pExecState;
	jsValue		m_nIndex;
};

using WebViewProvider = std::function<wkeWebView()>;
using Callback = std::function<jsValue(jsExecState)>;
using JsFuncProvideByCPP = std::unordered_map<std::string, Callback>;
class CCallCenter
{
public:
	CCallCenter() = delete;
	CCallCenter(WebViewProvider func) { m_funcWebViewProvider = std::move(func); };
	virtual ~CCallCenter() {};
	
	inline const char*	CallJs(const char* pszFuncName);
	template<typename... Args>
	inline const char*	CallJs(const char* pszFuncName, Args... args);

	inline void	ProvideJsFunc(const char* pszFuncName, std::function<void()> func);
#ifdef EXPAND_PARAMETER_LIST
	inline void	ProvideJsFunc(const char* pszFuncName, std::function<void(JsValueExtractor arg1)> func);
	inline void	ProvideJsFunc(const char* pszFuncName, std::function<void(JsValueExtractor arg1, JsValueExtractor arg2)> func);
	inline void	ProvideJsFunc(const char* pszFuncName, std::function<void(JsValueExtractor arg1, JsValueExtractor arg2, JsValueExtractor arg3)> func);
	inline void	ProvideJsFunc(const char* pszFuncName, std::function<void(JsValueExtractor arg1, JsValueExtractor arg2, JsValueExtractor arg3, JsValueExtractor arg4)> func);
#else
	inline void	ProvideJsFunc(const char* pszFuncName, int nArgc, std::function<void(std::vector<JsValueExtractor>&)> func);
#endif

protected:
	inline void	SaveJsFunc(const char* pszFuncName, int nArgc, std::function<jsValue(jsExecState)> callback);
	inline static jsValue WKE_CALL_TYPE wkeJsCall(jsExecState pExecState, void* param);
	
	template<typename T0>
	inline void F(std::vector<jsValue>& vec, jsExecState pExecState, T0 t);
	template<typename T, typename... Args>
	inline void F(std::vector<jsValue>& vec, jsExecState pExecState, T t, Args... args);

	inline jsValue to_js_value(jsExecState, bool value);
	inline jsValue to_js_value(jsExecState, int value);
	inline jsValue to_js_value(jsExecState, double value);
	inline jsValue to_js_value(jsExecState pExecState, const char *value);
	inline jsValue to_js_value(jsExecState pExecState, const wchar_t *value);
	inline jsValue to_js_value(jsExecState pExecState, const std::string &value);
	inline jsValue to_js_value(jsExecState pExecState, const std::wstring &value);

protected:
	WebViewProvider		m_funcWebViewProvider;
	JsFuncProvideByCPP	m_funcJsFuncProvideByCPP;
};
#include "CallCenter.cpp"
#endif