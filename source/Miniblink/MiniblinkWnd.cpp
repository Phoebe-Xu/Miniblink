﻿#include "MiniblinkWnd.h"
#include <assert.h>

CMiniblinkWnd::CMiniblinkWnd()
	: m_pWebView(nullptr)
	, m_bQuitOnClose(false)
	, m_funcResourceProvider(nullptr)
	, m_funcJsCppReady(nullptr)
	, m_funcConfirmation(nullptr)
{
}

CMiniblinkWnd::~CMiniblinkWnd()
{
}

bool CMiniblinkWnd::CreateWebView(wkeWindowType emType, HWND hParent, const RECT& rcWnd)
{
	bool bSuccess = false;
	if (bSuccess = (nullptr != (m_pWebView = wkeCreateWebWindow(emType, hParent, rcWnd.left, rcWnd.top, rcWnd.right - rcWnd.left, rcWnd.bottom - rcWnd.top))))
	{
		WCHAR szDir[MAX_PATH] = { 0 };
		GetTempPathW(__crt_countof(szDir), szDir);
		wkeSetLocalStorageFullPath(m_pWebView, szDir);
		wkeSetCookieJarFullPath(m_pWebView, szDir);
		wkeOnWindowDestroy(m_pWebView, &HandleWindowDestroy, this);
	}
	return bSuccess;
}

void CMiniblinkWnd::SetCaption(const std::wstring& strCaption)
{
	assert(nullptr != m_pWebView);
	if (nullptr != m_pWebView)
	{
		wkeSetWindowTitleW(m_pWebView, strCaption.c_str());
	}
}

void CMiniblinkWnd::SetResourceProvider(ResourceProvider funcProvider)
{
	assert(nullptr != m_pWebView);
	if (nullptr != m_pWebView)
	{
		m_funcResourceProvider = std::move(funcProvider);
		wkeOnLoadUrlBegin(m_pWebView, &HandleLoadUrlBegin, &m_funcResourceProvider);
	}
}

void CMiniblinkWnd::SetJsCppReady(JsCppReady funcReady/*= nullptr*/)
{
	assert(nullptr != m_pWebView);
	if (nullptr != m_pWebView)
	{
		m_funcJsCppReady = std::move(funcReady);
		wkeOnLoadingFinish(m_pWebView, &HandleJsCppReady, this);
	}
}

void CMiniblinkWnd::SetCloseConfirmation(CloseConfirmation funcConfirmation /*= nullptr*/)
{
	assert(nullptr != m_pWebView);
	if (nullptr != m_pWebView)
	{
		m_funcConfirmation = std::move(funcConfirmation);
		wkeOnWindowClosing(m_pWebView, &HandleWindowClosing, this);
	}
}

void CMiniblinkWnd::LoadUrl(const std::wstring& strUrl)
{
	assert(nullptr != m_pWebView);
	if (nullptr != m_pWebView)
	{
		wkeLoadURLW(m_pWebView, strUrl.c_str());
	}
}

void CMiniblinkWnd::SetQuitAppOnClose(bool bQuitApp)
{
	m_bQuitOnClose = bQuitApp;
}

void CMiniblinkWnd::SetVisible(bool bVisible)
{
	assert(nullptr != m_pWebView);
	if (nullptr != m_pWebView)
	{
		wkeShowWindow(m_pWebView, bVisible);
	}
}

wkeWebView CMiniblinkWnd::WebView()
{
	return m_pWebView;
}

bool CMiniblinkWnd::HandleLoadUrlBegin(wkeWebView pWebView, void* param, const char* pszUrl, wkeNetJob pJob)
{
	bool bSuccess = false;
	ResourceProvider* pProvider = reinterpret_cast<ResourceProvider*>(param);
	if (nullptr == pProvider || nullptr == *pProvider)
	{
		if (0 == _stricmp(pszUrl, "app:///index.html"))
		{
			const char szHtml[] = R"(
			<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<title>Call between JavaScript and C++</title>
					<style type = "text/css">
						body{
							margin : 5;
						}
						.bg{
							width:400px;
							height:300px;
							background:#FAFAFA;
						}
					</style>
				</head>
				<body>
					<div class = "bg">
						<p><center>Call between JavaScript and C++</center></p>
						<p><center>Click and Drag the mouse, the log show in console.</center></p>
						<label>Text from Cpp:<label/>
						<input id = "text">
						<input id = "btn" type = "button" value = "click me">
					</div>
				</body>
				<script>
					var bleftButtonDown = false;
					function setText(txt) {
						// C++ 将调用此JS
						document.getElementById('text').value = txt;
					}
					function getTitle() {
						// C++ 将调用此JS
						setTitle("Call between JavaScript and C++");
					}
					document.getElementById('btn').onclick = function() {

						getText();// 调用C++实现
						console.log("button id:" + obj.id + " click");
					}
					document.addEventListener("mousedown", function(){
						var e = event || window.event;
						if (0 == e.button) {
							bleftButtonDown = true;
							leftButtonDown(e.clientX, e.clientY);// 调用C++实现
							console.log("leftButtonDown:" + e.clientX + "," + e.clientY);
						}
					});
					document.addEventListener("mousemove", function(){
						var e = event || window.event;
						if (true == bleftButtonDown && 0 == e.button) {
							mouseMove(e.clientX, e.clientY);// 调用C++实现
							console.log("mousemove:" + e.clientX + "," + e.clientY);
						}
					});
					document.addEventListener("mouseup", function(){
						var e = event || window.event;
						if (0 == e.button) {
							bleftButtonDown = false;
							var e = event || window.event;
							leftButtonUp(e.clientX, e.clientY);// 调用C++实现
							console.log("leftButtonUp:" + e.clientX + "," + e.clientY);
						}
						else if(2 == e.button) {
							var e = event || window.event;
							rightButtonUp(e.clientX, e.clientY);// 调用C++实现
							console.log("rightButtonUp:" + e.clientX + "," + e.clientY);
						}
					});
				</script>
			</html>
				)";
			wkeNetSetData(pJob, const_cast<char *>(szHtml), static_cast<int>(sizeof(szHtml) - 1));
			bSuccess = true;
		}
	}
	else
	{
		bSuccess = pProvider->operator()(pWebView, pszUrl, pJob);
	}
	return bSuccess;
}

void CMiniblinkWnd::HandleJsCppReady(wkeWebView webView, void* param, const wkeString url, wkeLoadingResult result, const wkeString failedReason)
{
	CMiniblinkWnd* pThis = nullptr;
	if (nullptr != (pThis = reinterpret_cast<CMiniblinkWnd*>(param)))
	{
		if (nullptr != pThis->m_funcJsCppReady)
		{
			pThis->m_funcJsCppReady();
		}
	}
}

bool CMiniblinkWnd::HandleWindowClosing(wkeWebView pWebView, void* param)
{
	bool bClose = true;
	CMiniblinkWnd* pThis = nullptr;
	if (nullptr != (pThis = reinterpret_cast<CMiniblinkWnd*>(param)))
	{
		if (nullptr != pThis->m_funcConfirmation)
		{
			bClose = pThis->m_funcConfirmation();
		}
	}
	return bClose;
}

void CMiniblinkWnd::HandleWindowDestroy(wkeWebView pWebView, void* param)
{
	CMiniblinkWnd* pThis = nullptr;
	if (nullptr != (pThis = reinterpret_cast<CMiniblinkWnd*>(param)))
	{
		pThis->m_pWebView = nullptr;
		if (true == pThis->m_bQuitOnClose)
		{
			PostQuitMessage(0);
		}
	}
}
