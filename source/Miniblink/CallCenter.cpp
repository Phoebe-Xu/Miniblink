#ifdef CALL_CENTER_CPP
const char* CCallCenter::CallJs(const char* pszFuncName)
{
	const char* pszReturn = nullptr;
	wkeWebView pWebView = nullptr;
	if (nullptr != m_funcWebViewProvider && nullptr != (pWebView = m_funcWebViewProvider()))
	{
		jsExecState pExecState = wkeGlobalExec(pWebView);
		jsValue func = jsGetGlobal(pExecState, pszFuncName);
		jsValue valInput = jsString(pExecState, "");
		jsValue valOutput = jsCallGlobal(pExecState, func, &valInput, 1);
		pszReturn = jsToString(pExecState, valOutput);
	}
	return pszReturn;
}

template<typename... Args>
const char* CCallCenter::CallJs(const char* pszFuncName, Args... args)
{
	const char* pszReturn = nullptr;
	wkeWebView pWebView = nullptr;
	if (nullptr != m_funcWebViewProvider && nullptr != (pWebView = m_funcWebViewProvider()))
	{
		jsExecState pExecState = wkeGlobalExec(pWebView);
		jsValue func = jsGetGlobal(pExecState, pszFuncName);
		int nArgc = sizeof...(args);
		std::vector<jsValue> vecJsArgs;
		if (0 < nArgc)
		{
			vecJsArgs.reserve(nArgc);
			F(vecJsArgs, pExecState, args...);
		}
		jsValue valOutput = jsCall(pExecState, func, jsUndefined(), vecJsArgs.data(), static_cast<int>(vecJsArgs.size()));
		pszReturn = jsToString(pExecState, valOutput);
	}
	return pszReturn;
}

void CCallCenter::ProvideJsFunc(const char* pszFuncName, std::function<void()> func)
{
	SaveJsFunc(pszFuncName, 0, [func = std::move(func)](jsExecState pExecState)
	{
		// int nArgc = jsArgCount(pExecState);
		func();
		return jsUndefined();
	});
}

#ifdef EXPAND_PARAMETER_LIST
void CCallCenter::ProvideJsFunc(const char* pszFuncName, std::function<void(JsValueExtractor arg1)> func)
{
	SaveJsFunc(pszFuncName, 1, [func = std::move(func)](jsExecState pExecState)
	{
		func({ pExecState, 0 });
		return jsUndefined();
	});
}

void CCallCenter::ProvideJsFunc(const char* pszFuncName, std::function<void(JsValueExtractor arg1, JsValueExtractor arg2)> func)
{
	SaveJsFunc(pszFuncName, 2, [func = std::move(func)](jsExecState pExecState)
	{
		func({ pExecState, 0 }, { pExecState, 1 });
		return jsUndefined();
	});
}

void CCallCenter::ProvideJsFunc(const char* pszFuncName, std::function<void(JsValueExtractor arg1, JsValueExtractor arg2, JsValueExtractor arg3)> func)
{
	SaveJsFunc(pszFuncName, 3, [func = std::move(func)](jsExecState pExecState)
	{
		func({ pExecState, 0 }, { pExecState, 1 }, { pExecState, 2 });
		return jsUndefined();
	});
}

void CCallCenter::ProvideJsFunc(const char* pszFuncName, std::function<void(JsValueExtractor arg1, JsValueExtractor arg2, JsValueExtractor arg3, JsValueExtractor arg4)> func)
{
	SaveJsFunc(pszFuncName, 4, [func = std::move(func)](jsExecState pExecState)
	{
		func({ pExecState, 0 }, { pExecState, 1 }, { pExecState, 2 }, { pExecState, 3 });
		return jsUndefined();
	});
}
#else
void CCallCenter::ProvideJsFunc(const char* pszFuncName, int nArgc, std::function<void(std::vector<JsValueExtractor>&)> func)
{
	SaveJsFunc(pszFuncName, nArgc, [nArgc, func = std::move(func)](jsExecState pExecState)
	{
		std::vector<JsValueExtractor> vecValue;
		for (int i = 0; i < nArgc; ++i)
		{
			vecValue.push_back({ pExecState, i });
		}
		func(vecValue);
		return jsUndefined();
	});
}
#endif

void CCallCenter::SaveJsFunc(const char* pszFuncName, int nArgc, std::function<jsValue(jsExecState)> callback)
{
	Callback& savedCallback = (m_funcJsFuncProvideByCPP[pszFuncName] = std::move(callback));
	wkeJsBindFunction(pszFuncName, wkeJsCall, &savedCallback, nArgc);
}

jsValue WKE_CALL_TYPE CCallCenter::wkeJsCall(jsExecState pExecState, void* param)
{
	return reinterpret_cast<Callback*>(param)->operator()(pExecState);
}

template<typename T0>
void CCallCenter::F(std::vector<jsValue>& vec, jsExecState pExecState, T0 t)
{
	jsValue value = to_js_value(pExecState, t);
	vec.push_back(value);
}

template<typename T, typename... Args>
void CCallCenter::F(std::vector<jsValue>& vec, jsExecState pExecState, T t, Args... args)
{
	jsValue value = to_js_value(pExecState, t);
	vec.push_back(value);
	if (0 < sizeof...(args))
	{
		F(vec, pExecState, args...);
	}
}

jsValue CCallCenter::to_js_value(jsExecState, bool value)
{
	return jsBoolean(value);
}

jsValue CCallCenter::to_js_value(jsExecState, int value)
{
	return jsInt(value);
}

jsValue CCallCenter::to_js_value(jsExecState, double value)
{
	return jsDouble(value);
}

jsValue CCallCenter::to_js_value(jsExecState pExecState, const char *value)
{
	return jsString(pExecState, value);
}

jsValue CCallCenter::to_js_value(jsExecState pExecState, const wchar_t *value)
{
	return jsStringW(pExecState, value);
}

jsValue CCallCenter::to_js_value(jsExecState pExecState, const std::string &value)
{
	return jsString(pExecState, value.c_str());
}

jsValue CCallCenter::to_js_value(jsExecState pExecState, const std::wstring &value)
{
	return jsStringW(pExecState, value.c_str());
}

#endif //CALL_CENTER_CPP