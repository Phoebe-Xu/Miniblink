#include "MainWnd.h"
#include <QFile>
#include <QIntValidator>
#include <QMessageBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QKeyEvent>
#include "BusinessWnd.h"

#define TEXT_URL            "https://www.baidu.com/"
#define TEXT_LEFT           "560"
#define TEXT_TOP            "240"
#define TEXT_WIDTH          "800"
#define TEXT_HEIGHT         "600"

MainWnd::MainWnd(QWidget *parent)
    : QMainWindow(parent)
    , m_pEdtUrl(nullptr)
    , m_pEdtLeft(nullptr)
    , m_pEdtTop(nullptr)
    , m_pEdtWidth(nullptr)
    , m_pEdtHeight(nullptr)
{
    ui.setupUi(this);
    initUI();
    setDefaultValue();
}

MainWnd::~MainWnd()
{

}

void MainWnd::initUI()
{
    QWidget* pParent = ui.centralWidget;
    QVBoxLayout* pMainLayout = new QVBoxLayout(pParent);
    QHBoxLayout* pHBoxLayout = new QHBoxLayout(pParent);
    QVBoxLayout* pVBoxLayout = new QVBoxLayout(pParent);

    QHBoxLayout* pUrlLayout = new QHBoxLayout(pParent);
    QLabel* pLabelUrl = createWidget<QLabel>(pParent, tr("URL:"), "labelUrl");
    m_pEdtUrl = createWidget<QLineEdit>(pParent, "", "edtUrl");
    pUrlLayout->addWidget(pLabelUrl);
    pUrlLayout->addWidget(m_pEdtUrl);
    pVBoxLayout->addLayout(pUrlLayout);

    QHBoxLayout* pRectLayout = new QHBoxLayout(pParent);
    QLabel* pLabelLeft = createWidget<QLabel>(pParent, tr("left:"), "labelLeft");
    m_pEdtLeft = createWidget<QLineEdit>(pParent, "", "edtLeft");
    limitInput(m_pEdtLeft);
    QLabel* pLabelTop = createWidget<QLabel>(pParent, tr("top:"), "labelTop");
    m_pEdtTop = createWidget<QLineEdit>(pParent, "", "edtTop");
    limitInput(m_pEdtTop);
    QLabel* pLabelWidth = createWidget<QLabel>(pParent, tr("width:"), "labelWidth");
    m_pEdtWidth = createWidget<QLineEdit>(pParent, "", "edtWidth");
    limitInput(m_pEdtWidth);
    QLabel* pLabelHeight = createWidget<QLabel>(pParent, tr("height:"), "labelHeight");
    m_pEdtHeight = createWidget<QLineEdit>(pParent, "", "edtHeight");
    limitInput(m_pEdtHeight);
    pRectLayout->addWidget(pLabelLeft);
    pRectLayout->addWidget(m_pEdtLeft);
    pRectLayout->addWidget(pLabelTop);
    pRectLayout->addWidget(m_pEdtTop);
    pRectLayout->addWidget(pLabelWidth);
    pRectLayout->addWidget(m_pEdtWidth);
    pRectLayout->addWidget(pLabelHeight);
    pRectLayout->addWidget(m_pEdtHeight);
    pVBoxLayout->addLayout(pRectLayout);

    pHBoxLayout->addLayout(pVBoxLayout);
    QPushButton* pBtnShow = createWidget<QPushButton>(pParent, tr("Create MiniblinkWnd"), "btnCreate");
    connect(pBtnShow, &QPushButton::clicked, this, &MainWnd::onBtnCreate);
    pHBoxLayout->addWidget(pBtnShow);

    pMainLayout->addLayout(pHBoxLayout);
    pMainLayout->addStretch();

    QFile file(":/QtMiniblink/MainWnd.qss");
    if (true == file.open(QFile::ReadOnly))
    {
        setStyleSheet(file.readAll());
        file.close();
    }
}

void MainWnd::setDefaultValue()
{
    m_pEdtUrl->setText(TEXT_URL);
    m_pEdtLeft->setText(TEXT_LEFT);
    m_pEdtTop->setText(TEXT_TOP);
    m_pEdtWidth->setText(TEXT_WIDTH);
    m_pEdtHeight->setText(TEXT_HEIGHT);
}

void MainWnd::limitInput(QLineEdit* pEdit)
{
    if (nullptr != pEdit)
    {
        pEdit->setAttribute(Qt::WA_InputMethodEnabled, false);
        pEdit->installEventFilter(this);
        pEdit->setValidator(new QIntValidator(pEdit));
    }
}

template<typename T>
T* MainWnd::createWidget(QWidget* pParent, const QString& strText, const QString& strObjName)
{
    T* pWidget = new T(pParent);
    if (nullptr != pWidget)
    {
        pWidget->setText(strText);
        pWidget->setObjectName(strObjName);
    }
    return pWidget;
}

void MainWnd::onBtnCreate()
{
    QString strUrl = m_pEdtUrl->text();
    QString strLeft = m_pEdtLeft->text();
    QString strTop = m_pEdtTop->text();
    QString strWidth = m_pEdtWidth->text();
    QString strHeight = m_pEdtHeight->text();

    bool bParamReady = (false == strUrl.isEmpty() && false == strLeft.isEmpty() && false == strTop.isEmpty() && false == strWidth.isEmpty() && false == strHeight.isEmpty());
    if (false == bParamReady)
    {
        QMessageBox::information(this, tr("Tip"), tr("Incomplete parameters"), QMessageBox::StandardButton::Ok);
    }
    else
    {
        BusinessWnd wnd(this);
        if (true == wnd.create({ strLeft.toInt(), strTop.toInt(), strWidth.toInt(), strHeight.toInt() }))
        {
            wnd.loadUrl(strUrl);
            wnd.setVisible(true);
            wnd.exec();
        }
    }
}

bool MainWnd::eventFilter(QObject* pObj, QEvent* pEvent)
{
    static QList<QObject*> lstLineEdt = { m_pEdtUrl, m_pEdtLeft, m_pEdtTop, m_pEdtWidth, m_pEdtHeight };
    if (true == lstLineEdt.contains(pObj))
    {
        QKeyEvent* keyEvent = static_cast<QKeyEvent*>(pEvent);
        if (true == keyEvent->matches(QKeySequence::Paste))
        {
            return true;
        }
    }
    return QWidget::eventFilter(pObj, pEvent);
}