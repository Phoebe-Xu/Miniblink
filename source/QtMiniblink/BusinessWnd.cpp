#include "BusinessWnd.h"
#include <QJsonDocument>
#include "JsCpp.h"


BusinessWnd::BusinessWnd(QWidget *parent)
	: MiniblinkWnd(parent)
{
	registeJsCall(JSCALLCPP_GET_TEXT, 0, [this](const std::vector<QValue>& vec) { handleJsGetText(); });
}

BusinessWnd::~BusinessWnd()
{
}

void BusinessWnd::handleJsCppReady(wkeWebView webView, const wkeString url, wkeLoadingResult result, const wkeString failedReason)
{
	callJs(CPPCALLJS_GET_WND_CAPTION);
	callJs(CPPCALLJS_GET_WND_SIZE);
}

void BusinessWnd::handleJsGetText()
{
	QString strText = "";
	callJs(CPPCALLJS_SET_TEXT);
	callJsWithParam(CPPCALLJS_SET_TEXT, strText);
}

