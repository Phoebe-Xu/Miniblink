#pragma once
/*
* JSCALLCPP_XXX 为Web前端调用C++的功能名称
* CPPCALLJS_XXX 为C++后端调用Web前端的功能名称
* 功能接口只是表示所起的功能，真正的函数接口分别是：JsCallCpp 和 CppCallJs，Web前端只需要关注 JsCallCpp 即可。
* 举例：
* 获取Text：		JsCallCpp('{"function":"getText"}');
* 设置窗口尺寸：	JsCallCpp('{"function":"setWndSize", "param1":800, "param2":600}');
* 设置窗口标题：	JsCallCpp('{"function":"setWndCaption", "param1":"Caption"}');
* 设置窗口最小化：	JsCallCpp('{"function":"setWndMinimized"}');
* 设置窗口最大化：	JsCallCpp('{"function":"setWndMaximized"}');
* 设置窗口关闭：	JsCallCpp('{"function":"setWndClose"}');
* 通知打开 DevTools：JsCallCpp('{"function":"noticeShowDevTools"}');
* 
* 通过鼠标左键按下、移动、抬起来实现窗口从(100,110)移动到(200,220).
* 通知鼠标左键按下：JsCallCpp('{"function":"noticeWndLBtnDown", "param1":100, "param2":110}');
* 通知鼠标移动：	JsCallCpp('{"function":"noticeWndMouseMove", "param1":200, "param2":220}');
* 通知鼠标左键抬起：JsCallCpp('{"function":"noticeWndLBtnUp", "param1":200, "param2":220}');
* 
* 同理，C++调用Web前端的JavaScript脚本 CppCallJs, 参数仍然是 Json 字符串，function 表示功能名称，param1 param2 ...代表参数。
*/
#define		JSCALLCPP_GET_TEXT				"getText"

#define		CPPCALLJS_SET_TEXT				"setText"

