#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QtMiniblink.h"

class MainWnd : public QMainWindow
{
    Q_OBJECT

public:
    MainWnd(QWidget *parent = Q_NULLPTR);
    virtual ~MainWnd();

    void    initUI();
    void    setDefaultValue();
    void    limitInput(QLineEdit* pEdit);

protected:
    template<typename T>
    T*      createWidget(QWidget* pParent, const QString& strObjName, const QString& strText);
    void    onBtnCreate();

protected:
    bool    eventFilter(QObject* pObj, QEvent* pEvent);

private:
    Ui::QtMiniblinkClass    ui;
    QLineEdit*              m_pEdtUrl;
    QLineEdit*              m_pEdtLeft;
    QLineEdit*              m_pEdtTop;
    QLineEdit*              m_pEdtWidth;
    QLineEdit*              m_pEdtHeight;
};
