#pragma once
#include <QtWidgets/QDialog>
#include <QRect>
#include <QJsonDocument>
#include <QJsonObject>
#include <functional>
#include <tuple>
#include "wke.h"

#define JSCALLCPP_ROOT					"JsCallCpp"
#define CPPCALLJS_ROOT					"CppCallJs"
#define TEXT_FUNCTION_NAME				"function"
#define TEXT_PARAM						"param"
#define TEXT_TRUE						"true"
#define TEXT_ONE						"1"

#define JSCALLCPP_SET_WND_SIZE			"setWndSize"
#define JSCALLCPP_SET_WND_CAPTION		"setWndCaption"
#define JSCALLCPP_SET_WND_MINIMIZED		"setWndMinimized"
#define JSCALLCPP_SET_WND_MAXIMIZED		"setWndMaximized"
#define JSCALLCPP_SET_WND_CLOSE			"setWndClose"
#define JSCALLCPP_NOTICE_WND_LBTNDOWN	"noticeWndLBtnDown"
#define JSCALLCPP_NOTICE_WND_RBTNDOWN	"noticeWndRBtnDown"
#define JSCALLCPP_NOTICE_WND_MOUSEMOVE	"noticeWndMouseMove"
#define JSCALLCPP_NOTICE_WND_LBTNUP		"noticeWndLBtnUp"
#define JSCALLCPP_NOTICE_WND_RBTNUP		"noticeWndRBtnUp"
#define JSCALLCPP_NOTICE_SHOW_DEVTOOLS	"noticeShowDevTools"

#define CPPCALLJS_GET_WND_SIZE			"getWndSize"
#define CPPCALLJS_GET_WND_CAPTION		"getWndCaption"

class QValue
{
public:
	QValue() = delete;
	QValue(const QString& strValue) : m_strValue(strValue) {};
	virtual ~QValue() {};

	operator int() const { return m_strValue.toInt(); }
	operator const QString& () const { return m_strValue; }
	operator bool() const { return (0 == m_strValue.compare(TEXT_TRUE, Qt::CaseInsensitive) || 0 == m_strValue.compare(TEXT_ONE)); }

protected:
	QString		m_strValue;
};

using JS_CALL_FUNC = std::function<void(const std::vector<QValue>&)>;
using JS_CALL_MAP = std::map<std::string, std::tuple<int, JS_CALL_FUNC>>;

#define MACRO_MINIBLINK \
public: \
template<typename T> \
void F(QJsonObject& jsonObject, int nIndex, T t) \
{ \
	jsonObject.insert(QString("%1%2").arg(TEXT_PARAM).arg(nIndex), QString("%1").arg(t)); \
} \
 \
template<typename T, typename... Args> \
void F(QJsonObject& jsonObject, int nIndex, T t, Args... args) \
{ \
	F(jsonObject, nIndex, t); \
	if (0 < sizeof...(args)) \
		F(jsonObject, ++nIndex, args...); \
} \
template<typename... Args > \
void callJsWithParam(const QString& strFuncName, Args... args) \
{ \
	if (m_pWebView && !strFuncName.isEmpty()) { \
		QJsonDocument doc; \
		QJsonObject jsonObject = doc.object(); \
		jsonObject.insert(TEXT_FUNCTION_NAME, strFuncName); \
		if (0 < sizeof...(args)) \
			F(jsonObject, 1, args...); \
		doc.setObject(jsonObject); \
		QString strJson = doc.toJson(QJsonDocument::Compact); \
		jsExecState pExecState = wkeGlobalExec(m_pWebView); \
		jsValue func = jsGetGlobal(pExecState, CPPCALLJS_ROOT); \
		jsValue value = jsStringW(pExecState, strJson.toStdWString().c_str()); \
		jsCall(pExecState, func, jsUndefined(), &value, 1); \
	} \
}

class MiniblinkWnd : public QDialog
{
	Q_OBJECT

public:
	MiniblinkWnd(QWidget* pWnd);
	virtual ~MiniblinkWnd();

	bool		create(const QRect& rcWnd);
	void		loadUrl(const QString& strUrl);
	void		setVisible(bool bVisible);
	void		callJs(const QString& strFuncName);
	void		registeJsCall(const QString& strFuncName, int nArgc, JS_CALL_FUNC jsCall);

protected:
	virtual void				handleJsCppReady(wkeWebView webView, const wkeString url, wkeLoadingResult result, const wkeString failedReason);
	virtual bool				handleWindowClosing(wkeWebView pWebView);
	virtual void				handleWindowDestroy(wkeWebView pWebView);

protected:
	virtual void				handleJsCall(const QString& strJson);
	virtual void				handleJsSetWndSize(int nWidth, int nHeight);
	virtual void				handleJsSetWndCaption(const QString& strCaption);
	virtual void				handleJsSetWndMinimized();
	virtual void				handleJsSetWndMaximized();
	virtual void				handleJsSetWndClose();
	virtual void				handleJsNoticeWndLBtnDown(int nX, int nY);
	virtual void				handleJsNoticeWndRBtnDown(int nX, int nY);
	virtual void				handleJsNoticeWndMouseMove(int nX, int nY);
	virtual void				handleJsNoticeWndLBtnUp(int nX, int nY);
	virtual void				handleJsNoticeWndRBtnUp(int nX, int nY);
	virtual void				handleJsNoticeShowDevTools();

protected:
	HWND						hwnd();
	static jsValue WKE_CALL_TYPE wkeJsCall(jsExecState pExecState, void* param);
	static void WKE_CALL_TYPE	wkeJsCppReady(wkeWebView webView, void* param, const wkeString url, wkeLoadingResult result, const wkeString failedReason);
	static bool	WKE_CALL_TYPE	wkeWindowClosing(wkeWebView pWebView, void* param);
	static void	WKE_CALL_TYPE	wkeWindowDestroy(wkeWebView pWebView, void* param);

protected:
	wkeWebView					m_pWebView;
	QString						m_strUrl;
	bool						m_bQuitOnClose;
	JS_CALL_MAP					m_mapJsCall;

	bool						m_bLButtonDown;
	POINT						m_ptLeftBtnDown;
};
