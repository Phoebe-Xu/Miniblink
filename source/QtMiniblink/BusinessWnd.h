#pragma once

#include <QObject>
#include "MiniblinkWnd.h"

class BusinessWnd : public MiniblinkWnd
{
//	Q_OBJECT
	MACRO_MINIBLINK
public:
	BusinessWnd(QWidget*parent);
	virtual ~BusinessWnd();
	
protected:
	virtual void		handleJsCppReady(wkeWebView webView, const wkeString url, wkeLoadingResult result, const wkeString failedReason);

protected:
	void				handleJsGetText();
};
