#include "MiniblinkWnd.h"
#include <QApplication>
#include <QStandardPaths>
#include <QFileInfo>

#define TEXT_DEV_TOOLS_CAPTION			"DevTools"
#define DEV_TOOLS_WIDTH					800
#define DEV_TOOLS_HEIGHT				600
#define TEXT_DEV_TOOLS_PATH				L"/inspector/inspector.html"

MiniblinkWnd::MiniblinkWnd(QWidget* pWnd)
	: QDialog(pWnd)
	, m_pWebView(nullptr)
	, m_bQuitOnClose(true)
	, m_bLButtonDown(false)
	, m_ptLeftBtnDown {0,0}
{
	m_mapJsCall[JSCALLCPP_SET_WND_SIZE] = std::make_tuple(2, [this](const std::vector<QValue>& vec) {handleJsSetWndSize(vec[0], vec[1]); });
	m_mapJsCall[JSCALLCPP_SET_WND_CAPTION] = std::make_tuple(1, [this](const std::vector<QValue>& vec) {handleJsSetWndCaption(vec[0]); });
	m_mapJsCall[JSCALLCPP_SET_WND_MINIMIZED] = std::make_tuple(0, [this](const std::vector<QValue>& vec) {handleJsSetWndMinimized(); });
	m_mapJsCall[JSCALLCPP_SET_WND_MAXIMIZED] = std::make_tuple(0, [this](const std::vector<QValue>& vec) {handleJsSetWndMaximized(); });
	m_mapJsCall[JSCALLCPP_SET_WND_CLOSE] = std::make_tuple(0, [this](const std::vector<QValue>& vec) {handleJsSetWndClose(); });
	m_mapJsCall[JSCALLCPP_NOTICE_WND_LBTNDOWN] = std::make_tuple(2, [this](const std::vector<QValue>& vec) {handleJsNoticeWndLBtnDown(vec[0], vec[1]); });
	m_mapJsCall[JSCALLCPP_NOTICE_WND_RBTNDOWN] = std::make_tuple(2, [this](const std::vector<QValue>& vec) {handleJsNoticeWndRBtnDown(vec[0], vec[1]); });
	m_mapJsCall[JSCALLCPP_NOTICE_WND_MOUSEMOVE] = std::make_tuple(2, [this](const std::vector<QValue>& vec) {handleJsNoticeWndMouseMove(vec[0], vec[1]); });
	m_mapJsCall[JSCALLCPP_NOTICE_WND_LBTNUP] = std::make_tuple(2, [this](const std::vector<QValue>& vec) {handleJsNoticeWndLBtnUp(vec[0], vec[1]); });
	m_mapJsCall[JSCALLCPP_NOTICE_WND_RBTNUP] = std::make_tuple(2, [this](const std::vector<QValue>& vec) {handleJsNoticeWndRBtnUp(vec[0], vec[1]); });
	m_mapJsCall[JSCALLCPP_NOTICE_SHOW_DEVTOOLS] = std::make_tuple(0, [this](const std::vector<QValue>& vec) {handleJsNoticeShowDevTools(); });
}

MiniblinkWnd::~MiniblinkWnd()
{
}

bool MiniblinkWnd::create(const QRect& rcWnd)
{
	bool bSuccess = false;
	setGeometry(rcWnd);
	 if (NULL != (m_pWebView = wkeCreateWebWindow(WKE_WINDOW_TYPE_TRANSPARENT, (HWND)winId(), rcWnd.left(), rcWnd.top(), rcWnd.width(), rcWnd.height())))
	 {
		bSuccess = true;
		
		WCHAR szDir[MAX_PATH] = { 0 };
		GetTempPathW(__crt_countof(szDir), szDir);
		wkeSetLocalStorageFullPath(m_pWebView, szDir);
		wkeSetCookieJarFullPath(m_pWebView, szDir);

		wkeOnLoadingFinish(m_pWebView, &wkeJsCppReady, this);
		wkeOnWindowClosing(m_pWebView, &wkeWindowClosing, this);
		wkeOnWindowDestroy(m_pWebView, &wkeWindowDestroy, this);
		wkeJsBindFunction(JSCALLCPP_ROOT, wkeJsCall, this, 1);
	}
	return bSuccess;
}

void MiniblinkWnd::loadUrl(const QString& strUrl)
{
	if (NULL != m_pWebView)
	{
		m_strUrl = strUrl;
		wkeLoadURLW(m_pWebView, m_strUrl.toStdWString().c_str());
	}
}

void MiniblinkWnd::setVisible(bool bVisible)
{
	if (NULL != m_pWebView)
	{
		wkeShowWindow(m_pWebView, bVisible);
	}
}

void MiniblinkWnd::callJs(const QString& strFuncName)
{
	if (NULL != m_pWebView && false == strFuncName.isEmpty())
	{
		QJsonDocument doc;
		QJsonObject jsonObject = doc.object();
		jsonObject.insert(TEXT_FUNCTION_NAME, strFuncName);
		doc.setObject(jsonObject);
		QString strJson = doc.toJson(QJsonDocument::Compact);

		jsExecState pExecState = wkeGlobalExec(m_pWebView);
		jsValue func = jsGetGlobal(pExecState, CPPCALLJS_ROOT);
		jsValue value = jsStringW(pExecState, strJson.toStdWString().c_str());
		jsCall(pExecState, func, jsUndefined(), &value, 1);
	}
}

void MiniblinkWnd::registeJsCall(const QString& strFuncName, int nArgc, JS_CALL_FUNC jsCall)
{
	m_mapJsCall[strFuncName.toStdString()] = std::make_tuple(nArgc, std::move(jsCall));
}

void MiniblinkWnd::handleJsCppReady(wkeWebView webView, const wkeString url, wkeLoadingResult result, const wkeString failedReason)
{
}

bool MiniblinkWnd::handleWindowClosing(wkeWebView pWebView)
{
	return true;
}

void MiniblinkWnd::handleWindowDestroy(wkeWebView pWebView)
{
	close();
}

void MiniblinkWnd::handleJsCall(const QString& strJson)
{
	QJsonDocument doc = QJsonDocument::fromJson(strJson.toUtf8());
	const QJsonObject& obj = doc.object();
	if (true == obj.contains(TEXT_FUNCTION_NAME))
	{
		QString strFuncName = obj.value(TEXT_FUNCTION_NAME).toString();
		const int nParamCount = obj.count() - 1;

		std::vector<QValue> vecParam;
		QString strKey, strValue;
		for (int i = 1; i <= nParamCount; ++i)
		{
			strKey = QString("%1%2").arg(TEXT_PARAM).arg(i);
			if (true == obj.contains(strKey))
			{
				if (true == obj.value(strKey).isString())
				{
					strValue = obj.value(strKey).toString();
				}
				else if (true == obj.value(strKey).isDouble())
				{
					strValue = QString("%1").arg(obj.value(strKey).toDouble());
				}
				else if (true == obj.value(strKey).isBool())
				{
					strValue = QString("%1").arg(obj.value(strKey).toBool() ? 1 : 0);
				}
				else
				{
					strValue = QString("%1").arg(obj.value(strKey).toInt());
				}
			}
			vecParam.push_back(strValue);
			strValue.clear();
		}
		JS_CALL_MAP::iterator it = m_mapJsCall.find(strFuncName.toStdString());
		if (m_mapJsCall.end() != it && nParamCount == std::get<0>(it->second))
		{
			std::get<1>(it->second).operator()(vecParam);
		}
	}
}

void MiniblinkWnd::handleJsSetWndCaption(const QString& strCaption)
{
	if (NULL != m_pWebView)
	{
		wkeSetWindowTitleW(m_pWebView, strCaption.toStdWString().c_str());
	}
}

void MiniblinkWnd::handleJsSetWndSize(int nWidth, int nHeight)
{
	HWND hWnd = hwnd();
	if (TRUE == IsWindow(hWnd))
	{
		RECT rcWnd = { 0 };
		GetWindowRect(hWnd, &rcWnd);
		rcWnd = { rcWnd.left - (nWidth - rcWnd.right + rcWnd.left) / 2, rcWnd.top - (nHeight - rcWnd.bottom + rcWnd.top) / 2, nWidth, nHeight };
		setGeometry(QRect(rcWnd.left, rcWnd.top, rcWnd.right - rcWnd.left, rcWnd.bottom - rcWnd.top));
		SetWindowPos(hWnd, NULL, rcWnd.left, rcWnd.top, nWidth, nHeight, SWP_NOZORDER);
	}
}

void MiniblinkWnd::handleJsSetWndMinimized()
{
	HWND hWnd = NULL;
	if (NULL != m_pWebView && NULL != (hWnd = wkeGetWindowHandle(m_pWebView)))
	{
		PostMessageW(hWnd, WM_SYSCOMMAND, SC_MINIMIZE, NULL);
	}
}

void MiniblinkWnd::handleJsSetWndMaximized()
{
	HWND hWnd = NULL;
	if (NULL != m_pWebView && NULL != (hWnd = wkeGetWindowHandle(m_pWebView)))
	{
		PostMessageW(hWnd, WM_SYSCOMMAND, SC_MAXIMIZE, NULL);
	}
}

void MiniblinkWnd::handleJsSetWndClose()
{
	if (NULL != m_pWebView)
	{
		wkeDestroyWebView(m_pWebView);
	}
}

void MiniblinkWnd::handleJsNoticeWndLBtnDown(int nX, int nY)
{
	m_bLButtonDown = true;
	m_ptLeftBtnDown = { nX, nY };
	ClientToScreen(hwnd(), &m_ptLeftBtnDown);
}

void MiniblinkWnd::handleJsNoticeWndRBtnDown(int nX, int nY)
{
}

void MiniblinkWnd::handleJsNoticeWndMouseMove(int nX, int nY)
{
	if (true == m_bLButtonDown)
	{
		HWND hWnd = hwnd();
		POINT ptMove = { nX, nY };
		ClientToScreen(hWnd, &ptMove);
		RECT rcWnd;
		GetWindowRect(hWnd, &rcWnd);
		OffsetRect(&rcWnd, ptMove.x - m_ptLeftBtnDown.x, ptMove.y - m_ptLeftBtnDown.y);
		m_ptLeftBtnDown = ptMove;
		SetWindowPos(hWnd, NULL, rcWnd.left, rcWnd.top, rcWnd.right - rcWnd.left, rcWnd.bottom - rcWnd.top, SWP_NOSIZE);
	}
}

void MiniblinkWnd::handleJsNoticeWndLBtnUp(int nX, int nY)
{
	m_bLButtonDown = false;
}

void MiniblinkWnd::handleJsNoticeWndRBtnUp(int nX, int nY)
{
}

void WKE_CALL_TYPE onShowDevtoolsCallback(wkeWebView window, void* param)
{
	QString strTempDir = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
	wkeSetLocalStorageFullPath(window, strTempDir.toStdWString().c_str());
	wkeSetCookieJarFullPath(window, strTempDir.toStdWString().c_str());
	wkeSetWindowTitle(window, TEXT_DEV_TOOLS_CAPTION);
	wkeResizeWindow(window, DEV_TOOLS_WIDTH, DEV_TOOLS_HEIGHT);
	wkeMoveToCenter(window);
	const utf8* pszUrl = wkeGetURL(window);
	wkeLoadURL(window, pszUrl);
}

void MiniblinkWnd::handleJsNoticeShowDevTools()
{
	QString strPath = QString("%1%2").arg(QApplication::applicationDirPath(), TEXT_DEV_TOOLS_PATH);
	if (NULL != m_pWebView && true == QFileInfo::exists(strPath))
	{
		wkeShowDevtools(m_pWebView, strPath.toStdWString().c_str(), onShowDevtoolsCallback, nullptr);
	}
}

HWND MiniblinkWnd::hwnd()
{
	HWND hWnd = NULL;
	if (NULL != m_pWebView)
	{
		hWnd = wkeGetWindowHandle(m_pWebView);
	}
	return hWnd;
}

jsValue WKE_CALL_TYPE MiniblinkWnd::wkeJsCall(jsExecState pExecState, void* param)
{
	MiniblinkWnd* pThis = (MiniblinkWnd*)param;
	if (NULL != pThis)
	{
		QString strJson;
		const wchar_t* pszJson = jsToTempStringW(pExecState, jsArg(pExecState, 0));
		if (NULL != pszJson)
		{
			strJson = QString::fromWCharArray(pszJson);
		}
		pThis->handleJsCall(strJson);
	}
	return jsUndefined();
}

void MiniblinkWnd::wkeJsCppReady(wkeWebView webView, void* param, const wkeString url, wkeLoadingResult result, const wkeString failedReason)
{
	MiniblinkWnd* pThis = nullptr;
	if (NULL != (pThis = reinterpret_cast<MiniblinkWnd*>(param)))
	{
		pThis->handleJsCppReady(webView, url, result, failedReason);
	}
}

bool MiniblinkWnd::wkeWindowClosing(wkeWebView pWebView, void* param)
{
	bool bClose = true;
	MiniblinkWnd* pThis = nullptr;
	if (NULL != (pThis = reinterpret_cast<MiniblinkWnd*>(param)))
	{
		bClose = pThis->handleWindowClosing(pWebView);
	}
	return bClose;
}

void MiniblinkWnd::wkeWindowDestroy(wkeWebView pWebView, void* param)
{
	MiniblinkWnd* pThis = nullptr;
	if (NULL != (pThis = reinterpret_cast<MiniblinkWnd*>(param)))
	{
		pThis->handleWindowDestroy(pWebView);
	}
}