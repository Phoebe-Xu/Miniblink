#include "MainWnd.h"
#include <QtWidgets/QApplication>
#include <QTranslator>
#include "wke.h"
const QString MINIBLINK_DLL_NAME = "node.dll";
bool InitMiniblinkLibrary()
{
    bool bSuccess = false;
    HMODULE hMod = nullptr;// WKE_FOR_EACH_DEFINE_FUNCTION 里面要用到 hMod
    QString strPath = QString("%1/%2").arg(qApp->applicationDirPath(), MINIBLINK_DLL_NAME);
    FN_wkeInitializeEx wkeInitializeExFunc = nullptr;
    if (NULL != (hMod = LoadLibraryW(strPath.toStdWString().c_str()))
        && NULL != (wkeInitializeExFunc = (FN_wkeInitializeEx)GetProcAddress(hMod, "wkeInitializeEx")))
    {
        bSuccess = true;
        wkeInitializeExFunc(nullptr);

        WKE_FOR_EACH_DEFINE_FUNCTION(WKE_GET_PTR_ITERATOR0, WKE_GET_PTR_ITERATOR1, WKE_GET_PTR_ITERATOR2, WKE_GET_PTR_ITERATOR3, \
            WKE_GET_PTR_ITERATOR4, WKE_GET_PTR_ITERATOR5, WKE_GET_PTR_ITERATOR6, WKE_GET_PTR_ITERATOR9, WKE_GET_PTR_ITERATOR10, WKE_GET_PTR_ITERATOR11);
    }
    return bSuccess;
}
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QTranslator* translator = new QTranslator();
    translator->load("qtminiblink_zh.qm", ":/QtMiniblink/");
    app.installTranslator(translator);

    InitMiniblinkLibrary();
    MainWnd w;
    w.show();
    return app.exec();
}
